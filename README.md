# BIENVENIDOS
Este ejercicio practico les ayudará a entender la importancia de los repositorios en la nube como herramienta tecnologica de clase

## Caracteristicas

Podran realizar modificaciones a los documentos publicados en los repositorios con una trazabilidad entre publicaciones y evaluar el aporte que cada un de sus alumnos hace al proyecto, haciendo correcciones en linea y realizando comparativos entre versiones.

Espero le sirva y abra sus espectativas en el uso de repositorios

Cordial saludo


Yurly Andres Velez
Asesor de Tecnologia
yurly.andres.velez@gmail.com