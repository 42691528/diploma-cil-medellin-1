1930 - 1948 EL BOGOTAZO
------------------------
No existe actualmente un consenso sobre los orígenes del conflicto armado colombiano. De acuerdo con el informe presentado por la "Informe de la Comisión histórica del conflicto y sus víctimas" son múltiples las causas y factores de la confrontación histórica que se ha vivido en Colombia por más de cincuenta años. 

La violencia durante la primera mitad del siglo veinte se puede considerar la protogenesis del conflicto armado colombiano. Son dos grandes los campos en los que podemos enmarcar esta prehistoria de la confrontación armada en el país. El primero, tiene que ver con las reformas en torno al agro (Ley 200 de 1936 y Ley 100 de 1944). Pues con ellas se perfiló el problema de la concentración de tierras, uno de los principales factores causantes del conflicto en Colombia. El segundo, tiene que ver con con la lucha bipartidista entre liberales y conservadores durante la primera mita del siglo pasado. En efecto, las reformas impulsadas por los liberales en la década del treinta para instaurar plenamente su hegemonía y la respuesta en 1946 de un sector conservador contra ella, perfilaron un escenario de política y democracia restringida, en la que la conservación del poder fue pagada con la restricción de la participación política de grandes sectores de la sociedad colombiana.

Los orígenes del conflicto armado en Colombia son de corte político, resultado de la incapacidad de los grupos de poder de ampliar derechos democráticos y hacer del estado una esfera de dominio presente en todo el territorio nacional.  

El Bogotazo fue un episodio de violentas protestas, desórdenes y represión en el centro de Bogotá, la capital de Colombia, el 9 de abril de 1948, que siguieron al asesinato de Jorge Eliécer Gaitán.
1949 - 1958 DICTADURA
------------------------
1959 -1974 FRENTE NACIONAL
---------------------------
1975 - 1986 CONSOLIDACION GUERRILLAS Y LUCHA CONTRAINSURGENTE
------------------------------------------------------------
1987 - 1993 COORDINADORA GUERRILLERA, CONSTITUYENTE Y NARCOTRAFICO
-------------------------------------------------------------------
La Coordinadora Guerrillera Simón Bolívar fue un grupo que pretendía unificar el accionar de varias organizaciones guerrilleras en Colombia1 desde 1987 hasta principios de la década de 1990. La integraban las FARC, el M-19, el ELN, el EPL, el Partido Revolucionario de los Trabajadores y el Movimiento Armado Quintín Lame.2 Posteriormente, se retiraron de la Coordinadora las FARC y el ELN para continuar la lucha armada, luego que se desmovilizara la CGSB en 1991

1994 - 2005 NUEVAS NEGOCIACIONES Y PLAN COLOMBIA
------------------------------------------------------------
El proceso de paz entre el gobierno de Barco y el M-19 culmina exitosamente  con la firma  de un acuerdo político que contemplaba la necesidad de establecer un mecanismo institucional y legal que permitiera sacar adelante reformas urgentes que no solamente consolidaran el proceso de paz, sino que ampliara los espacios democráticos. El mecanismo que finalmente se adoptó a  fin de cumplir estos propósitos fue la convocatoria de una Asamblea  Nacional Constituyente. En dicho acuerdo de paz se expresaba que:

“Coincidimos en la necesidad de que a través de los mecanismos ordinarios de Reforma Constitucional o mediante la convocatoria del Constituyente Primario, Plebiscito, Referéndum o Asamblea Constituyente, se fortalezca la legitimidad institucional y la aprobación de materias de vital importancia para la consecución de la paz. Nos comprometemos a promover los acuerdos políticos necesarios para alcanzar este cometido de ampliación de los espacios democráticos. De todas maneras, la decisión que se tome en este sentido deberá estar precedida de un amplio acuerdo que incluya el asentimiento de quienes ejerzan la Presidencia de la República.”3

En el marco del acuerdo político de paz,   es preciso destacar también el hecho de permitir la posibilidad de la creación de una circunscripción especial de paz para eventuales partidos que surgieran de los procesos de desmovilización por  una sola vez, de tal manera,  que tanto la convocatoria a la asamblea nacional constituyente;  como la circunscripción especial de paz,  se constituye  en dos elementos políticos fundantes de una nueva cultura en el tránsito de la guerra a la política  en Colombia.

“Para promover la incorporación a la vida civil de los guerrilleros y su tránsito de la lucha armada a la vida política, se comprometen a respaldar el establecimiento, por una sola vez, de una Circunscripción Especial de Paz para partidos políticos surgidos de movimientos alzados en armas desmovilizados y reincorporados a la vida civil. Dicha circunscripción para Senado de la República y Cámara de Representantes se aplicará, a más tardar, en las elecciones de 1992, y sus características se definirán entre los signatarios en posterior acuerdo.”4

El 2 de agosto de 1990 el entonces director del liberalismo Cesar Gaviria, pocos días antes de la toma de posesión como nuevo presidente de los colombianos, junto a Antonio Navarro Wolf de la Alianza Democrática -M19 (AD-M19), Álvaro Villegas en representación del Conservatismo y Rodrigo Marín Bernal por el Movimiento de Salvación Nacional que orientaba el ex rehén del M-19, doctor Álvaro Gómez Hurtado, suscribieron un documento a fin de convocar una Asamblea Nacional Constituyente5. En dicho documento se  consagraba la posibilidad para que en un futuro ingresaran otros movimientos insurgentes  que realizaran procesos de paz. Este nuevo acuerdo político se plasmó posteriormente en el decreto de Estado de Sitio 1926 de 1990, que convoco la Asamblea Nacional Constituyente (A.N.C), estableciendo la fecha de elecciones, el temario y el sistema de elección de los delegatarios.
Año 1998. 

Político.
A comienzos del año, en cinco municipios del departamento del meta, no fue posible la posesión de sus alcaldes debido a que la guerrilla no permitió la realización de las elecciones.
En las elecciones parlamentarias del ocho de marzo la Orinoquia colombiana se quedó sin senadores que la representen y gestionen ante el gobierno nacional.
Un poco más de 460.000 votos le dieron la victoria a Andrés Pastrana en la segunda vuelta electoral que lo enfrentó con el liberal Horacio Serpa.
Plan Colombia.
Dentro de la estrategia de internacionalizar el conflicto, el presidente Andrés Pastrana lanzó el llamado Plan Colombia. Con este programa, el gobierno fue ante la comunidad internacional en busca de apoyo económico para enfrentar el narcotráfico.
El costo del Plan Colombia se estimó en 7.000 millones de dólares de los cuales el gobierno norteamericano se comprometió con aportar 1.300, el gobierno colombiano 4.000 millones de dólares y los recursos restantes saldrían de la cooperación de la comunidad internacional a través de las denominadas mesas de donantes.
El Plan Colombia que impulsó el presidente Pastrana tenía entre sus componentes: la búsqueda de la paz vía negociación, la lucha contra el narcotráfico que implicó el fortalecimiento en equipos y capacidad operativa del Ejército y las fumigaciones. Otro propósito era recuperar 700.000 hectáreas de agricultura pérdidas en el conflicto y mantener una tasa de crecimiento económico.

Conflicto.

La Batalla de la quebrada El Billar, Caquetá( 1 de marzo),  fue un ataque de las Fuerzas Armadas Revolucionarias de Colombia (FARC) previo a las elecciones parlamentarias y presidenciales de ese año. El batallón contraguerrilla No. 52 de la Brigada Móvil No. 3, conformado por 153 hombres, fue el encargado de combatir a los frentes 14 y 15 y a la columna 'Teófilo Forero' de las Farc por tres días. El resultado del ataque fueron 64 soldados muertos, 19 heridos y 43 secuestrados.
Masacre en Puerto Alvira- Mapiripán (4 mayo), fue dado por la incursión de un grupo de paramilitares el lunes 4 de mayo fue una de las más sangrientas y bárbaras en todo el país. En el hecho murieron por lo menos 19 personas y cuatro resultaron heridas.  Como consecuencia de la masacre, se presentó un éxodo de campesinos que huyeron del lugar hacia Villavicencio, Mapiripán y Puerto Inírida. 

Toma de La Uribe (4 de agosto) fue un ataque perpetrado por las Fuerzas Armadas Revolucionarias de Colombia (FARC) contra el cuartel de la Policía y una base militar, en el municipio de La Uribe, departamento del Meta. El ataque realizado por 4 frentes de las FARC, se saldó con graves pérdidas en las filas de las Fuerzas gubernamentales. Sin embargo, los subversivos fracasaron en su intento por ocupar las posiciones enemigas y tuvieron que retirarse de La Uribe, aunque siguieron hostigando los alrededores de la localidad durante tres días.
La Batalla de Tamborales (14 de agosto). fue un ataque perpetrado por las FARC en el corregimiento de Puerto Lleras en jurisdicción de Riosucio (Chocó). Las tropas del Ejército Nacional se hallaban adelantando labores de búsqueda y rastreo en el área, con el objetivo de rescatar a 7 uniformados secuestrados el 3 de agosto anterior en Pavarando (Antioquia), cuando se vieron atacados por unos 4 frentes del Bloque José María Cordova de las FARC. El batallón debió retroceder con grandes pérdidas. Resultado del combate perdieron la vida 42 militares y 21 más fueron secuestrados, que posteriormente serían incluidos en el grupo de los Canjeables.
Toma de Mitú- VAUPÉS (1 de noviembre), llamada Operación Marquetalia, fue un ataque perpetrado por las FARC, la toma dura 67 horas, en las cuales el grupo subversivo mantuvo totalmente el control militar de la capital del Vaupés, 16 policías resultaron muertos, 63 desaparecidos y 41 lograron sobrevivir.

Diálogos de paz 

Diálogos de paz entre el gobierno Pastrana y el ELN,
Fue una serie de conversaciones y negociaciones entre el gobierno del presidente colombiano, Andrés Pastrana y la guerrilla del Ejército de Liberación Nacional (ELN), iniciadas desde la elección de Pastrana como presidente de Colombia en el año 1998 y finalizadas el 7 de agosto de 2002.
El accionar de los paramilitares de las Autodefensas Unidas de Colombia golpeó al ELN en zonas donde tradicionalmente tenía presencia. El ELN culpó en varias ocasiones al gobierno de haber sido negligentes frente al fenómeno del paramilitarismo en Colombia.
Diálogos de paz entre el gobierno Pastrana y las FARC
El Proceso de Paz o Negociaciones de paz en Colombia se refiere al intento por parte del grupo guerrillero y el gobierno del presidente Andrés Pastrana para comenzar un proceso de paz y terminar con el conflicto armado colombiano. El proceso de paz fue llevado a cabo entre 1998 y 2002, con gestiones de negociaciones iniciadas en el año 1997. El proceso de diálogo estuvo acompañado por gobiernos de varios países. Los diálogos de paz tuvieron varios inconvenientes, empezando por los roces del ministro de defensa, altos mandos militares con el presidente Pastrana y la decisión de otorgar una zona desmilitarizada en la región de El Caguán para llevar ahí los diálogos con las FARC, sin un cese al fuego generalizado. 

Economía 
La recesión más profunda del 98-99 El último trimestre de 1998 marcó el comienzo de la peor recesión en la historia de Colombia, que llevó a que 1999 por primera vez se registrara un crecimiento negativo del 4,5 por ciento en el Producto Interno Bruto, PIB. Entre las causas figuran el excesivo endeudamiento del sector público y el endeudamiento del sector privado que con unas tasas de interés excesivamente altas que tocaron niveles del 50 y 60 por ciento hicieron las veces de aspiradoras de los recursos de la economía.
Colapsó el sistema hipotecario: muere el Upac.
Paralelamente a la crisis financiera el sistema hipotecario colapsó. El Upac entró en crisis y el gobierno adoptó medidas para darle alivios a los deudores que se encontraban en mora. Paralelamente, diseñó un programa de reducción automática de tasas de interés para los deudores al día.
Cambio en el sistema de salud.
De una sólo institución prestadora del servicio obligatorio de salud e igualmente en la administración de las pensiones se pasó a una amplia oferta. Nuevos agentes llegaron al mercado: EPS, IPS, ARPs, AFPs que desataron una fuerte competencia para ganarle clientes al Seguro Social que mantenía el monopolio.


Internacional.
Hugo Chávez Frías gana las elecciones presidenciales venezolanas
Bill Clinton se ve envuelto en un escándalo de grandes roporciones al conocerse su relación Monica Lewinsky.
Augusto Pinochet es capturado en Londres cuando se recuperaba de una operación y a los pocos días el gobierno español lo pide en extradición a Gran Bretaña

Cultura.
Se estrena la película “La vendedora de Rosas” de Víctor Gaviria.
Fernando Botero dona a Colombia más de 100 obras de su autoría y 60 de algunos de los más representativos creadores de la historia del arte de finales del siglo XIX y del XX. 

Año 1992.

Político 

En medio de la expectativa de todo un país y de la comunidad internacional, se instala en San Vicente del Caguán la mesa de negociación entre el gobierno y las Farc. El presidente Pastrana acude puntual, pero Manuel Marulanda Vélez, jefe de las Farc, nunca llega.
El 25 de enero un fuerte sismo sacude al Eje Cafetero y en pocos segundos acaba con la vida y las ilusiones de muchos colombianos humildes. El triste balance es de más de 1.000 muertos, 400.000 damnificados y millonarias pérdidas.
El 13 de agosto el país amanece de luto por la noticia del asesinato de Jaime Garzón. No sólo muere Garzón, sino también los entrañables personajes que él había creado.
El 14 de abril guerrilleros del ELN obligan al piloto de un avión de Avianca a aterrizar en una pista improvisada en Vijagual, Bolívar, secuestrando a pasajeros y tripulantes.

Conflicto 
La guerra entre paramilitares y grupos guerrilleros se recrudeció. El paramilitarismo se extendió en casi todo el país, asesinó a más de 500 personas y causó desplazamiento de poblaciones.
La guerrilla impuso las retenciones masivas o pescas milagrosas, estableciendo retenes en las carreteras o como ocurrió el 30 de mayo en la iglesia La María, de Cali y con el avión de Avianca, el 12 de abril, cuando fueron secuestrados sus 46 ocupantes que viajaban de Bucaramanga a Bogotá.

Económico 
Colombia finaliza el siglo XX sumida en una profunda recesión. De ser uno de los países más prósperos de la región, con un crecimiento económico superior al 4,5 por ciento en el primer quinquenio de la década, se pasó a menos del 3,0, presentándose este año una caída del PIB superior al 3,0 por ciento.
La tasa de desempleo superó el 20 por ciento. Bajó el consumo y se quebraron empresas grandes, medianas y pequeñas, afectadas por las altas tasas de interés y la revaluación de la moneda.
Muchos colombianos perdieron sus casas al no poder pagar las cuotas del Upac, causando una grave crisis en el sector de la construcción, uno de los mayores generadores de empleo.

Internacional 

El canal ya es de Panamá Con la firma del acta protocolaria entre la presidenta de Panamá, Mireya Moscoso, y el ex presidente de E.U. Jimmy Carter, culminaron el 14 de diciembre, formalmente, 96 años de soberanía estadounidense en el Canal de Panamá.


Año 200 

El 18 de diciembre entra en funcionamiento Transmilenio, una novedosa alternativa que pretende descongestionar las calles de la capital. Los buses rojos se convierten en la nueva cara del transporte público en Bogotá.

Internacional
El Concorde estalló en llamas a los pocos minutos de abandonar el aeropuerto Charles de Gaulle en París.
118 marinos rusos mueren encerrados en el submarino Kursk. El presidente Vladimir Putin pierde apoyo popular tras conocerse que ha mantenido en secreto el accidente en vez de solicitar ayuda a otros países.
El candidato republicano, George W. Bush, gana las elecciones presidenciales más polémicas en la historia de EE.UU.

------------------------------------------------
2006 -2016 LEY DE JUSTICIA Y PAZ; PROCESO DE NEGOCIACIÓN
--------------------------------------------------------
